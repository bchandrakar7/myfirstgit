git fetch origin
for reBranch in $(git branch -a)
do
{
  if [[ $reBranch == remotes/origin* ]];
  then
  {
    if [[ $reBranch ==remotes/origin/HEAD ]]; then 
    echo "HEAD is not a branch"
    else
      branch=$(echo $reBranch | cut -d'/' -f 3)
      echo $branch
      sha=$(git rev-parse origin/$branch)
      dateo=$(git show -s --format=%ci $sha)
      datef=$(echo $dateo | cut -d' ' -f 1)
      Todate=$(date -d "$datef" +'%s')
      current=$(date +'%s')
      day=$(( ( $current - $Todate )/60/60/24 ))
      echo $day
      if [ "$day" -gt 365 ]; then
      git push origin :$branch
      echo "delete the old branch $branch"
      fi
    fi

  }
  fi
}
done